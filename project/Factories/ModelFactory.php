<?php

namespace Prj\Factories;

use Prj\Contracts\DataInterface;
use Prj\Exceptions\ModelFactoryException;

class ModelFactory
{
    /**
     * Create instance of UserDataInterface
     *
     * @param array $data
     * @param $modelClassName
     * @return DataInterface
     * @throws ModelFactoryException
     */
    public function createModel(array $data, $modelClassName): DataInterface
    {
        if (class_exists($modelClassName)) {
            return new $modelClassName($data);
        }

        throw new ModelFactoryException("Class: $modelClassName does not exist!");
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws ModelFactoryException
     */
    public static function __callStatic($name, $arguments)
    {
        if (! method_exists(self::class, $name)) {
            throw new ModelFactoryException("Method: $name does not exist!");
        }

        return (new static)->$name(...$arguments);
    }
}