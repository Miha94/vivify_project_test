<?php

namespace Prj\Exceptions;

use Exception;

class ModelFactoryException extends \Exception
{
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}