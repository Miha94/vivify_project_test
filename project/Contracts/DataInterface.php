<?php

namespace Prj\Contracts;

interface DataInterface
{
    /**
     * @param $attribute
     * @param $value
     * @return mixed
     */
    public function setAttribute($attribute, $value);

    /**
     * @param $attribute
     * @return mixed
     */
    public function getAttribute($attribute);
}