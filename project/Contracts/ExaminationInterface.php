<?php

namespace Prj\Contracts;

interface ExaminationInterface
{
    /**
     * Create random results of examination
     *
     * @return mixed
     */
    public function createRandomResults();

    /**
     * Get all previously created results
     *
     * @return mixed
     */
    public function getResults();
}