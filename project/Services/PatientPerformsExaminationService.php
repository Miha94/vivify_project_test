<?php

namespace Prj\Services;

use Prj\Contracts\ExaminationInterface;
use Prj\Models\Users\Patient;

class PatientPerformsExaminationService
{
    /**
     * @var Patient
     */
    private $patient;

    /**
     * @var ExaminationInterface
     */
    private $examination;

    /**
     * PatientPerformsExaminationService constructor
     *
     * @param Patient $patient
     * @param ExaminationInterface $examination
     */
    public function __construct(Patient $patient, ExaminationInterface $examination)
    {
        $this->patient      = $patient;
        $this->examination  = $examination;
    }

    /**
     * @return mixed
     */
    public function perform()
    {
        // TODO check if examination already have different patient for appointment

        $this->examination->getAttribute('date') ?: $this->examination->setAttribute('date', date('Y-m-d'));
        $this->examination->getAttribute('time') ?: $this->examination->setAttribute('date', date('H:i:s'));

        Logger::getInstance()->log("Patient {$this->patient->getAttribute('name')} Successfully did " . get_class($this->examination));

        return $this->examination->createRandomResults()->getResults();
    }
}