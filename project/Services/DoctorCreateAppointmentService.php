<?php

namespace Prj\Services;

use Prj\Models\Examination;
use Prj\Models\Users\Doctor;
use Prj\Models\Users\Patient;

class DoctorCreateAppointmentService
{
    /**
     * @var Doctor
     */
    private $doctor;

    /**
     * @var Patient
     */
    private $patient;

    /**
     * @var Examination
     */
    private $examination;

    /**
     * DoctorCreateAppointmentService constructor
     *
     * @param Doctor $doctor
     * @param Patient $patient
     * @param Examination $examination
     */
    public function __construct(Doctor $doctor, Patient $patient, Examination $examination)
    {
        $this->doctor       = $doctor;
        $this->patient      = $patient;
        $this->examination  = $examination;
    }

    public function create($date, $time)
    {
        $this->examination->setAttribute('date', $date);
        $this->examination->setAttribute('time', $time);
        $this->examination->setAttribute('patient', $this->patient);
        $this->examination->setAttribute('doctor', $this->doctor);

        return $this->examination;
    }
}