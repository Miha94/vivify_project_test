<?php

namespace Prj\Services;

use Prj\Models\Users\Patient;
use Prj\Models\Users\Doctor;

class PatientChooseDoctor
{
    /**
     * @var Patient
     */
    private $patient;

    /**
     * PatientChooseDoctor constructor
     *
     * @param Patient $patient
     */
    public function __construct(Patient $patient)
    {
        $this->patient  = $patient;
    }

    /**
     * Create patient's doctor and return patient instance
     *
     * @param Doctor $doctor
     * @return Patient
     */
    public function choose(Doctor $doctor)
    {
        $this->patient->setAttribute('doctor', $doctor);

        Logger::getInstance()->log("Patient {$this->patient->getAttribute('name')} choose {$doctor->getAttribute('name')} for his Doctor");

        return $this->patient;
    }
}