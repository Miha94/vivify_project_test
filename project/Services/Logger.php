<?php

namespace Prj\Services;

use Prj\Exceptions\LoggerException;

class Logger
{
    private static $instance;

    /**
     * Private construct so no one can instantiate this class anywhere
     *
     * Logger constructor.
     */
    private function __construct()
    {
    }

    /**
     * Gets instance of the Logger
     *
     * @return Logger instance
     */
    public function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Logger();
        }

        return self::$instance;
    }

    /**
     * Adds a message to the log file
     *
     * @param String $message Message to be logged
     * @param string $file
     * @throws LoggerException
     */
    public function log($message, $file = __DIR__ . '/../log.txt')
    {
        try {
            $file = fopen($file, 'a') or die('Unable to open file');

            $message = "[" . date('Y-m-d') . " " . date('H:i:s') . "] {$message} \n";

            fwrite($file, $message);

            fclose($file);
        } catch (\Exception $e) {
            throw new LoggerException($e->getMessage());
        }
    }

    /**
     * Returns logs from file
     *
     * @param string $file
     * @return string
     * @throws LoggerException
     */
    public function get_logs($file = __DIR__ . '/../log.txt')
    {
        try {
            return file($file);
        } catch (\Exception $e) {
            throw new LoggerException($e->getMessage());
        }
    }
};