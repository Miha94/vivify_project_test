<?php

namespace Prj\Models\Users;

use Prj\Models\User;

class Patient extends User
{
    /**
     * @var
     */
    protected $jmbg;

    /**
     * @var
     */
    protected $healthCardNumber;

    /**
     * @var Doctor
     */
    protected $doctor;
}