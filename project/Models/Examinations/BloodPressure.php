<?php

namespace Prj\Models\Examinations;

use Prj\Models\Examination;

class BloodPressure extends Examination
{
    /**
     * @var
     */
    protected $topValue;

    /**
     * @var
     */
    protected $bottomValue;

    /**
     * @var
     */
    protected $heartbeat;

    public function createRandomResults()
    {
        $this->topValue     = rand(0, 100);
        $this->bottomValue  = rand(0, 100);
        $this->heartbeat    = rand(50, 150);

        return $this;
    }

    public function getResults()
    {
        return [
            'Top Value'     => $this->topValue,
            'Bottom Value'  => $this->bottomValue,
            'Hart Beat'     => $this->heartbeat
        ];
    }
}