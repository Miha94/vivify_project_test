<?php

namespace Prj\Models\Examinations;

use Prj\Models\Examination;

class BloodSugarLevel extends Examination
{
    /**
     * @var
     */
    protected $value;

    /**
     * @var
     */
    protected $lastMealTime;

    function createRandomResults()
    {
        $this->value        = rand(0, 100);
        $this->lastMealTime = '2018-03-03 08:00:00';

        return $this;
    }

    public function getResults()
    {
        return [
            'Value'         => $this->value,
            'Last Meal Time'=> $this->lastMealTime
        ];
    }
}