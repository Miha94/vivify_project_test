<?php

namespace Prj\Models;

use Prj\Contracts\DataInterface;
use Prj\Exceptions\ModelException;

class Model implements DataInterface
{
    /**
     * Model constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $key => $val) {
            $this->setAttribute($key, $val);
        }
    }

    public function setAttribute($attribute, $value)
    {
        if (property_exists($this, $attribute)) {
            $this->$attribute = $value;
        } else {
            throw new ModelException('Unknown attribute: ' . $attribute);
        }

        return $this;
    }

    public function getAttribute($attribute)
    {
        if (property_exists($this, $attribute)) {
            return $this->$attribute;
        } else {
            throw new ModelException('Unknown attribute: ' . $attribute);
        }
    }
}