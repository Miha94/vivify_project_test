<?php

namespace Prj\Models;

use Prj\Contracts\ExaminationInterface;
use Prj\Models\Users\Doctor;
use Prj\Models\Users\Patient;

abstract class Examination extends Model implements ExaminationInterface
{
    /**
     * @var
     */
    protected $date;

    /**
     * @var
     */
    protected $time;

    /**
     * @var Doctor;
     */
    protected $doctor;

    /**
     * @var Patient
     */
    protected $patient;
}