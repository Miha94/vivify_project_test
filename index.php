<?php

require_once __DIR__ . '/vendor/autoload.php';

use \Prj\Factories\ModelFactory;
use \Prj\Models\Users\Doctor;
use \Prj\Models\Users\Patient;
use \Prj\Services\Logger;
use \Prj\Services\DoctorCreateAppointmentService;
use \Prj\Models\Examinations\BloodPressure;
use \Prj\Services\PatientPerformsExaminationService;
use \Prj\Models\Examinations\BloodSugarLevel;
use \Prj\Services\PatientChooseDoctor;

// Create Logger instance
    $logger = Logger::getInstance();

// Create Doctor instance
    $doctor = ModelFactory::createModel(['name' => 'Milan'], Doctor::class);
    $logger->log("Doctor {$doctor->getAttribute('name')} created");

// Create Patient instace
    $patient= ModelFactory::createModel(['name' => 'Dragan'], Patient::class);
    $logger->log("Patient {$patient->getAttribute('name')} created");

// Dragan choose doctor
    $patient = (new PatientChooseDoctor($patient))->choose($doctor);

// Create BloodPressure instance
    $bloodPressureExamination = (new DoctorCreateAppointmentService($doctor, $patient, new BloodPressure([])))->create(date('Y-m-d'), date('H:i:s'));

// Show Dragan's blood sugar results
    echo "<pre>";
    var_dump((new PatientPerformsExaminationService($patient, new BloodSugarLevel(['patient' => $patient])))->perform());
    echo "</pre>";

// Show Dragan's blood pressure
    echo "<pre>";
    var_dump((new PatientPerformsExaminationService($patient, $bloodPressureExamination))->perform());
    echo "</pre>";

// Print every log
    echo "<pre>";
    var_dump($logger->get_logs());
    echo "</pre>";